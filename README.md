# Deep Learning Project

# Useful Links

## N-ODE

 - https://arxiv.org/pdf/1806.07366.pdf

### Notes for report
 - https://github.com/kmkolasinski/deep-learning-notes/tree/master/seminars/2019-03-Neural-Ordinary-Differential-Equations
 - https://github.com/rtqichen/torchdiffeq
 - https://www.youtube.com/watch?v=jltgNGt8Lpg&ab_channel=YannicKilcher
 - https://www.youtube.com/watch?v=AD3K8j12EIE&ab_channel=SirajRaval
 - https://www.youtube.com/watch?v=uPd0B0WhH5w&ab_channel=AndriyDrozdyuk
 - https://cs.stanford.edu/~ambrad/adjoint_tutorial.pdf
 - https://www.youtube.com/watch?v=MX1RJELWONc
 - http://www-users.med.cornell.edu/~jdvicto/jdv/mathcourse1617/mathcourse1617_Drover.pdf
 - https://voletiv.github.io/docs/presentations/20200710_Mila_Neural_ODEs_tutorial_Vikram_Voleti.pdf
 - https://www.cs.toronto.edu/~rtqichen/pdfs/neural_ode_slides.pdf
 - https://jontysinai.github.io/jekyll/update/2019/01/18/understanding-neural-odes.html
 - cnf paper https://arxiv.org/pdf/1810.01367.pdf
 - latent paper https://proceedings.neurips.cc/paper/2019/file/42a6845a557bef704ad8ac9cb4461d43-Paper.pdf
 - https://odr.chalmers.se/bitstream/20.500.12380/256887/1/256887.pdf
 - https://arxiv.org/pdf/2002.08071.pdf
 - useful link in the useful like https://github.com/Zymrael/awesome-neural-ode
 - https://crossminds.ai/video/neural-ordinary-differential-equations-for-continuous-normalizing-flows-60774d76825a3436b95eed89/
 - https://github.com/YuliaRubanova/latent_ode
 - https://faculty1.coloradocollege.edu/~sburns/toolbox/ODE_II.html

## space stuff
 - https://re.public.polimi.it/retrieve/handle/11311/1063150/309502/FURFR01-18.pdf
 - https://indico.esa.int/event/111/contributions/396/attachments/482/527/Optimal_real_time_landing_using_deep_networks.pdf
 - https://arxiv.org/pdf/1810.08719.pdf


## Car stuff

### Survey 

 - https://onlinelibrary.wiley.com/doi/epdf/10.1002/rob.21918

### Supervised 

 - https://arxiv.org/pdf/1604.07316.pdf 


### Formula RL

 - https://arxiv.org/pdf/2104.11106.pdf

### Rally 

 - https://arxiv.org/pdf/1807.02371.pdf
 - https://www.cc.gatech.edu/~bboots3/files/nips17drl.pdf

## Control

 - https://www.youtube.com/watch?v=5P7I-xPq8u8&ab_channel=ArxivInsights
 - https://arxiv.org/pdf/1707.06347.pdf

### Asynchronous RL

 - https://arxiv.org/pdf/1602.01783.pdf#page=10&zoom=100,0,0

## Reinforcement Learning Notes 

 - http://rail.eecs.berkeley.edu/deeprlcourse/
 - https://www.youtube.com/watch?v=zR11FLZ-O9M&t=3036s&ab_channel=LexFridman


